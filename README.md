# Sweatter



## Spring Boot learning

1. Spring Boot: делаем простое веб приложение на Java (простой сайт)

2. Spring Boot JPA (Hibernate): добавляем базу данных в веб приложение на Java (простой сайт)

3. Spring Boot Security: добавляем регистрацию и авторизацию пользователей в приложение (простой сайт)

4. Spring Boot Jpa (Hibernate): добавляем связи между таблицами базы данных (one to many)

5. Spring Boot Freemarker: подключаем шаблонизатор Freemarker

6. Spring Boot Security: добавляем панель администратора и роли пользователей, ограничиваем доступ

7. Spring Boot MVC: загрузка файлов на сервер и раздача статики

8. Spring Boot: оформляем UI с Bootstrap

9. Spring Boot Mail: рассылка почты пользователям, активация аккаунта

10. Spring Boot FlyWay: миграции БД, профиль пользователя

11. Spring Boot: bean validation, шифрование паролей

12. Spring Boot: reCaptcha, rest client, rememberMe и сохранение сессий в БД

13. Spring Boot: JPA oneToMany - сообщения пользователя, редактор сообщений

14. Spring Boot: JPA ManyToMany - подписки и подписчики

15. Spring Boot: постраничное отображение длинных списков (pagination)

16. Spring Boot: добавляем "лайки", используем HQL (JPQL)

17. Spring Boot + Turbolinks: ускоряем отображение страниц

18. Spring Boot: интеграционное тестирование с Spring Testing

19. Spring Boot: модульное тестирование и подмена (mock object)
