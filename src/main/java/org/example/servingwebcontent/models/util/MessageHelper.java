package org.example.servingwebcontent.models.util;

import org.example.servingwebcontent.models.User;

public abstract class MessageHelper {
    public static String getAuthorName(User author){
        return author != null ? author.getUsername() : "<none>";
    }
}
