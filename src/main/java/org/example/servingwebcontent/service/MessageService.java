package org.example.servingwebcontent.service;

import org.example.servingwebcontent.models.Message;
import org.example.servingwebcontent.models.User;
import org.example.servingwebcontent.models.dto.MessageDto;
import org.example.servingwebcontent.repos.MessageRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class MessageService {

    @Autowired
    private MessageRepo messageRepo;

    public Page<MessageDto> messageList(Pageable pageable, String search, User user){
        if(search != null && !search.isEmpty()){
            return messageRepo.findByTag(search, pageable, user);
        } else {
            return messageRepo.findAll(pageable, user);
        }
    }

    public Page<MessageDto> messageListForUser(Pageable pageable, User currentUser, User author) {
        return messageRepo.findByUser(pageable, author, currentUser);
    }
}
