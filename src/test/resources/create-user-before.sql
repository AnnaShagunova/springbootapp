delete from user_role;
delete from usr;

insert into usr(id, username, password, active) values
                                                    (1, 'w', '$2a$08$DJrJq2nhVhKVx6hTFMj8bueZLGUAz81SXr/fczl8wcHt0KhD37ylW', true),
                                                    (2, '1', '$2a$08$ZcIRO8VBHXD3A3YwU6jTZe8l36xDq1eNfJTgHqPNcpJnVLpyJSyZi', true);

insert into user_role(user_id, roles) values
                                          (1, 'ADMIN'), (1, 'USER'),
                                          (2, 'USER');